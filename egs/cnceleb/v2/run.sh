#!/bin/bash

SPEAKER_TRAINER_ROOT=../../..
CN_celeb1_path=/work102/lilt/database/cnc_v2/CN-Celeb_flac
CN_celeb2_path=/work102/lilt/database/cnc_v2/CN-Celeb2_flac
musan_path=/work102/lilt/database/musan
rirs_path=/work102/lilt/database/RIRS_NOISES

nnet_type=ResNet34_half
pooling_type=TSP
loss_type=amsoftmax
embedding_dim=256
scale=30.0
margin=0.1
cuda_device=1,2,3

stage=0

if [ $stage -le 0 ];then
  # flac to wav
  python3 $SPEAKER_TRAINER_ROOT/steps/flac2wav.py \
          --dataset_dir $CN_celeb1_path/data \
          --speaker_level 1

  python3 $SPEAKER_TRAINER_ROOT/steps/flac2wav.py \
          --dataset_dir $CN_celeb2_path/data \
          --speaker_level 1
 
  python3 $SPEAKER_TRAINER_ROOT/steps/flac2wav.py \
          --dataset_dir $CN_celeb1_path/eval/enroll \
          --speaker_level 0
  
  python3 $SPEAKER_TRAINER_ROOT/steps/flac2wav.py \
          --dataset_dir $CN_celeb1_path/eval/test \
          --speaker_level 0
fi


if [ $stage -le 1 ];then
  # prepare data
  if [ ! -d data/wav ]; then
    mkdir -p data/wav
  fi

  mkdir -p data/wav/train
  for spk in `cat ${CN_celeb1_path}/dev/dev.lst`;do
    ln -s ${CN_celeb1_path}/data/${spk} data/wav/train/$spk
  done

  for spk in `cat ${CN_celeb2_path}/spk.lst`;do
    ln -s ${CN_celeb2_path}/data/${spk} data/wav/train/$spk
  done

  mkdir -p data/trials
  python3 local/format_trials_cnceleb.py \
          --cnceleb_root $CN_celeb1_path \
          --dst_trl_path data/trials/CNC-Eval-Core.lst
fi


if [ $stage -eq 2 ];then
  # compute VAD for each dataset
  echo Compute VAD $cnceleb1_root
  python3 $SPEAKER_TRAINER_ROOT/steps/compute_vad.py \
          --data_dir $CN_celeb1_path/data \
          --extension wav \
          --speaker_level 1 \
          --num_jobs 40

  echo Compute VAD $cncceleb2_root
  python3 $SPEAKER_TRAINER_ROOT/steps/compute_vad.py \
          --data_dir $$CN_celeb2_path/data \
          --extension wav \
          --speaker_level 1 \
          --num_jobs 40
fi


if [ $stage -le 3 ];then
  # prepare data for model training
  mkdir -p data
  echo Build train list
  python3 $SPEAKER_TRAINER_ROOT/steps/build_datalist.py \
          --data_dir data/wav/train \
          --extension wav \
          --speaker_level 1 \
          --data_list_path data/train_lst.csv

  echo Build $musan_path list
  python3 $SPEAKER_TRAINER_ROOT/steps/build_datalist.py \
          --data_dir $musan_path \
          --extension wav \
          --data_list_path data/musan_lst.csv

  echo Build $rirs_path list
  python3 $SPEAKER_TRAINER_ROOT/steps/build_datalist.py \
          --data_dir $rirs_path \
          --extension wav \
          --data_list_path data/rirs_lst.csv
fi


if [ $stage -le 4 ];then
  # model training
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --train_list_path data/train_lst.csv \
          --trials_path data/trials/CNC-Eval-Core.lst \
          --n_mels 80 \
          --max_frames 201 --min_frames 200 \
          --batch_size 200 \
          --nPerSpeaker 1 \
          --max_seg_per_spk 500 \
          --num_workers 40 \
          --max_epochs 61 \
          --loss_type $loss_type \
          --nnet_type $nnet_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --learning_rate 0.01 \
          --lr_step_size 3 \
          --lr_gamma 0.50 \
          --margin $margin \
          --scale $scale \
          --eval_interval 5 \
          --eval_frames 0 \
          --save_top_k 20 \
          --distributed_backend dp \
          --reload_dataloaders_every_epoch \
          --gpus 3
fi


if [ $stage -le 5 ];then
  # evaluation
  ckpt_path=exp/*/*.ckpt

  mkdir -p scores/
  echo Evaluate CNC-Eval-Core
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --evaluate \
          --checkpoint_path $ckpt_path \
          --n_mels 80 \
          --trials_path data/trials/CNC-Eval-Core.lst \
          --scores_path scores/CNC-Eval-Core.foo \
          --train_list_path data/train_list.csv \
          --nnet_type $nnet_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --scale $scale \
          --margin $margin \
          --num_workers 20 \
          --gpus 1
fi

