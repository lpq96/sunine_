from logging import log
import torch
import torch.nn.functional as F
from .basecam import *
import seaborn as sns
import matplotlib.pyplot as plt
import math
import numpy as np
import copy

class Occlusion_Speech(BaseCAM):

    """
        ScoreCAM, inherit from BaseCAM
        ScoreCAM for speech domain 
        input: log-mel specturm
        model: the last layer of Resnet network 
    """

    def __init__(self, model_dict):
        super().__init__(model_dict)

    def forward(self, input, occluding_size, occluding_pixel, occluding_stride, class_idx=None, retain_graph=False):
        c, height, width = input.size()  # b:1,c:3,h:224,w:224
        
        # predication on raw input
        logit = self.model_arch(input).cuda()
        if class_idx is None:
            predicted_class = logit.max(1)[-1]
            score = logit[:, logit.max(1)[-1]].squeeze().requires_grad_()

        else:
            predicted_class = torch.LongTensor([class_idx])
            score = logit[:, class_idx].squeeze()
        
        logit = F.softmax(logit,1)

        if torch.cuda.is_available():
          predicted_class = predicted_class.cuda()
          score = score.cuda()
          logit = logit.cuda()

        self.model_arch.zero_grad()

        output_height = int(math.ceil((height-occluding_size) / occluding_stride + 1))
        output_width = int(math.ceil((width-occluding_size) / occluding_stride + 1))
        heatmap = np.zeros((output_height, output_width))
        for h in range(output_height):
            for w in range(output_width):
              # Occluder region:
              h_start = h * occluding_stride
              w_start = w * occluding_stride
              h_end = min(height, h_start + occluding_size)
              w_end = min(width, w_start + occluding_size)
              # Getting the image copy, applying the occluding window and classifying it again:
              input_image = copy.deepcopy(input)
              input_image[:,h_start:h_end, w_start:w_end] =  occluding_pixel
              plt.figure()
              plt.imshow(input_image[0,:,:].cpu().numpy())
              plt.savefig('occluding_{},{}.png'.format(h_start,w_start), bbox_inches='tight')
              logit = self.model_arch(input_image).cuda()
              logit = F.softmax(logit,1)
              score = logit[:, predicted_class].squeeze().requires_grad_()
              # print(logit.max(1)[-1])
              # print('scanning position (%s, %s)'%(h,w))
              heatmap[h,w] = 1-score
        fig = plt.figure()
        heatmap = torch.from_numpy(heatmap).cuda()
        heatmap.type(torch.FloatTensor).cuda()
        heatmap = heatmap.unsqueeze(0).unsqueeze(0)
        heatmap = F.interpolate(heatmap, size=(height, width), mode='bilinear', align_corners=False)
        print(heatmap.size())
        heatmap = heatmap.squeeze(0).squeeze(0)

        sns.heatmap(heatmap.type(torch.FloatTensor).cpu(),cmap='rainbow')
        # if save_path is not None:
        plt.savefig('occlusion_heat_map.png')  
        # heatmap.squeeze(0)
        print(heatmap.size())
        heatmap = heatmap.unsqueeze(0)

        return heatmap
    def __call__(self, input, occluding_size, occluding_pixel, occluding_stride, class_idx=None, retain_graph=False):
        return self.forward(input, occluding_size, occluding_pixel, occluding_stride, class_idx=None, retain_graph=False)