import torch
from torch import nn
from tqdm import tqdm
from scipy.ndimage.filters import gaussian_filter
import sys
import numpy as np
import matplotlib.pyplot as plt

from trainer.metric import score

# from utils import *

# self.HW = 224 * 224 # image area
# n_classes = 1000

def gkern(klen, nsig):
    """Returns a Gaussian kernel array.
    Convolution with it results in image blurring."""
    # create nxn zeros
    inp = np.zeros((klen, klen))
    # set element at the middle to one, a dirac delta
    inp[klen//2, klen//2] = 1
    # gaussian-smooth the dirac, resulting in a gaussian filter mask
    k = gaussian_filter(inp, nsig)
    kern = np.zeros((1, 1, klen, klen))
    # kern[0] = k
    kern[0, 0] = k
    # kern[1, 1] = k
    # kern[2, 2] = k
    return torch.from_numpy(kern.astype('float32'))

def auc(arr):
    """Returns normalized Area Under Curve of the array."""
    return (arr.sum() - arr[0] / 2 - arr[-1] / 2) / (arr.shape[0] - 1)

class CausalMetric():

    def __init__(self, model, loss, input_size, mode, step, substrate_fn):
        r"""Create deletion/insertion metric instance.

        Args:
            model (nn.Module): Black-box model being explained.
            mode (str): 'del' or 'ins'.
            step (int): number of pixels modified per one iteration.
            substrate_fn (func): a mapping from old pixels to new pixels.
        """
        assert mode in ['del', 'ins']
        self.model = model
        self.loss = loss
        self.mode = mode
        self.step = step
        self.substrate_fn = substrate_fn
        self.H = input_size[0] 
        self.W = input_size[1]

    def forward_SID(self,input,label):
        logit = self.model(input)
        logit = logit.unsqueeze(0)
        output = self.loss(logit,label)
        # # output = self.softmax(output) # logit.size() -> [1,400]
        # output = output.cpu().detach().numpy().tolist() # max(x[0]) is the max posterior
        # label_int = label.cpu().numpy().tolist()[0]
        # score = output[0][label]
        # # print(score)
        # # x[0].sort(reverse=True)
        # # print(x[0])
        # y_tilde = output[0].index(max(output[0]))
        # # print(label_int, y_tilde)
        return output
    def single_run(self, img_tensor, label, explanation, verbose=0, save_to=None):
        r"""Run metric on one image-saliency pair.

        Args:
            img_tensor (Tensor): normalized image tensor.torch.Size([1, 3, 224, 224])
            explanation (np.ndarray): saliency map.
            verbose (int): in [0, 1, 2].
                0 - return list of scores.
                1 - also plot final step.
                2 - also plot every step and print 2 top classes.
            save_to (str): directory to save every step plots to.

        Return:
            scores (nd.array): Array containing scores at every step.
        """
        pred = self.forward_SID(img_tensor,label)
        # pred = self.model(img_tensor.cuda())
        top, c = torch.max(pred, 1) # c 是类别; top 是 对应c 类别的分数
        c = c.cpu().numpy()[0]  
        n_steps = (self.H*self.W + self.step - 1) // self.step

        if self.mode == 'del':
            title = 'Deletion game'
            ylabel = 'Pixels deleted'
            start = img_tensor.clone()
            finish = self.substrate_fn(img_tensor)
        elif self.mode == 'ins':
            title = 'Insertion game'
            ylabel = 'Pixels inserted'
            blur_input = img_tensor.unsqueeze(1) 
            start = self.substrate_fn(blur_input.cpu())
            start = start.squeeze(1)
            finish = img_tensor.clone()
        scores = np.empty(n_steps + 1)

        #! Coordinates of pixels in order of decreasing saliency (降序)
        salient_order = np.flip(np.argsort(explanation.reshape(-1, self.H*self.W), axis=1), axis=-1)
        print(salient_order.shape)
        # np.argsort 将input按照axis排序(升序)，并返回排序后的下标
        # np.flip  沿指定的轴反转数组元素的顺序，从而保留数组的形状(变成降序)
        for i in range(n_steps+1):
            # pred = self.model(start.cuda())
            pred = self.forward_SID(start.cuda(),label) # pred.shape [1,5994]
            pr, cl = torch.topk(pred, 2) # 选择 pred中 的topN pr是分数output [1,2] cl是对应分数的类[1,2]
            if verbose == 2:
                print('{}: {}'.format(cl[0][0], float(pr[0][0])))
                print('{}: {:.3f}'.format(cl[0][1], float(pr[0][1])))
            scores[i] = pred[0, c]
            print(scores[i])
            # Render image if verbose, if it's the last step or if save is required.
            if verbose == 2 or (verbose == 1 and i == n_steps) or save_to:
                plt.figure(figsize=(10, 5))
                plt.subplot(121)
                plt.title('{} {:.1f}%, P={:.4f}'.format(ylabel, 100 * i / n_steps, scores[i]))
                plt.axis('off')
                # tensor_imshow(start[0])
                plt.imshow(start[0].cpu().numpy())


                plt.subplot(122)
                plt.plot(np.arange(i+1) / n_steps, scores[:i+1])
                plt.xlim(-0.1, 1.1)
                plt.ylim(0, 1.05)
                plt.fill_between(np.arange(i+1) / n_steps, 0, scores[:i+1], alpha=0.4)
                plt.title(title)
                plt.xlabel(ylabel)
                plt.ylabel('input_pre_class is {}'.format(c))
                if save_to:
                    plt.savefig(save_to + '/{:03d}.png'.format(i))
                    plt.close()
                else:
                    plt.show()
            if i < n_steps:
                # salient_order 是 所有pixel 的 importance(saliency)的程度排序(降序)
                coords = salient_order[:, self.step * i:self.step * (i + 1)] # coords (1,self.step)
                # import ipdb; ipdb.set_trace()
                # 把finish的形状reshape成 (1,3,HW),把finish的 importance对应放到start 的上面。
                #! 下面这句话有一个bug  他的原理是 tensor转化为numpy后共享内存。才可以进行以下的操作。 原理是由torch.from_numpy将numpy转为tensor后，tensor.numpy()将tensor转为numpy来共享内存 
                # start.cpu().numpy().reshape(1, 3, self.H*self.W)[0, :, coords] = finish.cpu().numpy().reshape(1, 3, self.H*self.W)[0, :, coords]
                #
                start_numpy = start.cpu().numpy()
                start_numpy.reshape(1, self.H*self.W)[0,coords] = finish.cpu().numpy().reshape(1,self.H*self.W)[0,coords]
                start = torch.from_numpy(start_numpy.reshape(1, self.H, self.W))
        return scores

    # def evaluate(self, img_batch, exp_batch, batch_size):
    #     r"""Efficiently evaluate big batch of images.

    #     Args:
    #         img_batch (Tensor): batch of images.
    #         exp_batch (np.ndarray): batch of explanations.
    #         batch_size (int): number of images for one small batch.

    #     Returns:
    #         scores (nd.array): Array containing scores at every step for every image.
    #     """
    #     n_samples = img_batch.shape[0]
    #     predictions = torch.FloatTensor(n_samples, n_classes)
    #     assert n_samples % batch_size == 0
    #     for i in tqdm(range(n_samples // batch_size), desc='Predicting labels'):
    #         preds = self.model(img_batch[i*batch_size:(i+1)*batch_size].cuda()).cpu()
    #         predictions[i*batch_size:(i+1)*batch_size] = preds
    #     top = np.argmax(predictions, -1)
    #     n_steps = (self.H*self.W + self.step - 1) // self.step
    #     scores = np.empty((n_steps + 1, n_samples))
    #     salient_order = np.flip(np.argsort(exp_batch.reshape(-1, self.H*self.W), axis=1), axis=-1)
    #     r = np.arange(n_samples).reshape(n_samples, 1)

    #     substrate = torch.zeros_like(img_batch)
    #     for j in tqdm(range(n_samples // batch_size), desc='Substrate'):
    #         substrate[j*batch_size:(j+1)*batch_size] = self.substrate_fn(img_batch[j*batch_size:(j+1)*batch_size])

    #     if self.mode == 'del':
    #         caption = 'Deleting  '
    #         start = img_batch.clone()
    #         finish = substrate
    #     elif self.mode == 'ins':
    #         caption = 'Inserting '
    #         start = substrate
    #         finish = img_batch.clone()

    #     # While not all pixels are changed
    #     for i in tqdm(range(n_steps+1), desc=caption + 'pixels'):
    #         # Iterate over batches
    #         for j in range(n_samples // batch_size):
    #             # Compute new scores
    #             preds = self.model(start[j*batch_size:(j+1)*batch_size].cuda())
    #             preds = preds.cpu().numpy()[range(batch_size), top[j*batch_size:(j+1)*batch_size]]
    #             scores[i, j*batch_size:(j+1)*batch_size] = preds
    #         # Change specified number of most salient pixels to substrate pixels
    #         coords = salient_order[:, self.step * i:self.step * (i + 1)]
    #         start.cpu().numpy().reshape(n_samples, 3, self.H*self.W)[r, :, coords] = finish.cpu().numpy().reshape(n_samples, 3, self.H*self.W)[r, :, coords]
    #     print('AUC: {}'.format(auc(scores.mean(1))))
    #     return scores
