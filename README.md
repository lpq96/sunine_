# Sunine_: Visualiazaiton with many methods on Speaker Classification Tasks.


# Speaker Trainer toolkit

This repository is used to implement the state-of-the-art speaker recognition models.

## Dependencies

```
pip install -r requirements.txt
```

## Apply VAD

```bash
# usage
# Webrtc VAD
python3 steps/compute_vad.py \
              --data_dir ${your_dataset_dir_path} \
              --num_jobs 30
```

## Data preparation

```bash
# usage
python3 steps/build_datalist.py \
              --extension vad \
              --dataset_dir ${your_dataset_dir_path} \
              --data_list_path data_list.csv
```

## Example 

```
# Here we supply two examples:

- egs/voxceleb
cd egs/voxceleb/{v1,v2}
sh run.sh

- egs/cnceleb
cd egs/cnceleb/{v1,v2}
sh run.sh
```

## Tensorboard
monitor the training process.
```
tensorboard --logdir runs/* --bind_all
```
