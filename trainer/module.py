#!/usr/bin/env python
# encoding: utf-8

import os
from argparse import ArgumentParser
import numpy as np
import pandas as pd

import torch
from torch import tensor
from torch.autograd.grad_mode import no_grad
import torch.nn as nn
from pytorch_lightning import LightningModule, Trainer
# from torch.nn import functional as F
import torch.nn.functional as F

from torch.utils.data import DataLoader

from pytorch_lightning.callbacks import ModelCheckpoint
import torchaudio
from tqdm import tqdm

import importlib
from collections import OrderedDict
from .metric import cosine_score, PLDA_score
from .metric.plda import PldaAnalyzer

from .utils import PreEmphasis
from .dataset_loader import Train_Dataset, Train_Sampler, Test_Dataset, Dev_Dataset, Test_classification_Dataset

# from .cam.cam import CAM, GradCAM, GradCAMpp, SmoothGradCAMpp, ScoreCAM, LayerCAM
from .cam.cam_give_gt import CAM, GradCAM, GradCAMpp, SmoothGradCAMpp, ScoreCAM, LayerCAM

from .cam import *
# from .cam.Occlusion import *
import sys
import matplotlib.pyplot as plt

class Model(LightningModule):
    def __init__(self, **kwargs):
        super().__init__()
        self.save_hyperparameters()

        # load trials and data list
        if os.path.exists(self.hparams.trials_path):
            self.trials = np.loadtxt(self.hparams.trials_path, dtype=str)
        if os.path.exists(self.hparams.train_list_path):
            df = pd.read_csv(self.hparams.train_list_path)
            speaker = np.unique(df["utt_spk_int_labels"].values)
            self.hparams.num_classes = len(speaker)
            print("Number of Training Speaker classes is: {}".format(self.hparams.num_classes))

        # Network information Report
        print("Network Type: ", self.hparams.nnet_type)
        print("Pooling Type: ", self.hparams.pooling_type)
        print("Embedding Dim: ", self.hparams.embedding_dim)

        #########################
        ### Network Structure ###
        #########################

        # 1. Acoustic Feature
        self.mel_trans = torch.nn.Sequential(
                PreEmphasis(),
                torchaudio.transforms.MelSpectrogram(sample_rate=16000, n_fft=512, 
                    win_length=400, hop_length=160, window_fn=torch.hamming_window, n_mels=self.hparams.n_mels)
                )
        self.instancenorm = nn.InstanceNorm1d(self.hparams.n_mels)

        # 2. Speaker_Encoder
        Speaker_Encoder = importlib.import_module('trainer.nnet.'+self.hparams.nnet_type).__getattribute__('Speaker_Encoder')
        self.speaker_encoder = Speaker_Encoder(**dict(self.hparams))

        # 3. Loss / Classifier
        if not self.hparams.evaluate:
            LossFunction = importlib.import_module('trainer.loss.'+self.hparams.loss_type).__getattribute__('LossFunction')
            self.loss = LossFunction(**dict(self.hparams))
        self.softmax = nn.Softmax()

    def forward(self, x, label):
        x = self.extract_speaker_embedding(x)
        x = x.reshape(-1, self.hparams.nPerSpeaker, self.hparams.embedding_dim)
        loss, acc = self.loss(x, label)
        return loss.mean(), acc

    def extract_speaker_embedding(self, data):
        x = data.reshape(-1, data.size()[-1])
        x = self.mel_trans(x) + 1e-6
        x = x.log()
        x = self.instancenorm(x)
        x = self.speaker_encoder(x)
        return x

    def training_step(self, batch, batch_idx):
        data, label = batch
        loss, acc = self(data, label)
        tqdm_dict = {"acc":acc}
        self.log('train_loss', loss)
        self.log('train_acc', acc)
        output = OrderedDict({
            'loss': loss,
            'progress_bar': tqdm_dict,
            'log': tqdm_dict
            })
        return output

    def train_dataloader(self):
        frames_len = np.random.randint(self.hparams.min_frames, self.hparams.max_frames)
        print("Chunk size is: ", frames_len)
        print("Augment Mode: ", self.hparams.augment)
        train_dataset = Train_Dataset(self.hparams.train_list_path, self.hparams.augment, 
                musan_list_path=self.hparams.musan_list_path, rirs_list_path=self.hparams.rirs_list_path,
                max_frames=frames_len)
        train_sampler = Train_Sampler(train_dataset, self.hparams.nPerSpeaker,
                self.hparams.max_seg_per_spk, self.hparams.batch_size)
        loader = torch.utils.data.DataLoader(
                train_dataset,
                batch_size=self.hparams.batch_size,
                num_workers=self.hparams.num_workers,
                sampler=train_sampler,
                pin_memory=True,
                drop_last=False,
                )
        return loader

    def test_dataloader(self, trials):
        enroll_list = np.unique(trials.T[1])
        test_list = np.unique(trials.T[2])
        eval_list = np.unique(np.append(enroll_list, test_list))
        print("number of eval: ", len(eval_list))
        print("number of enroll: ", len(enroll_list))
        print("number of test: ", len(test_list))

        test_dataset = Test_Dataset(data_list=eval_list, eval_frames=self.hparams.eval_frames, num_eval=0)
        loader = DataLoader(test_dataset, num_workers=self.hparams.num_workers, batch_size=1)
        return loader
        
    def test_dataloader_for_classification(self):
        print(self.hparams.test_list_path)
        test_dataset = Test_classification_Dataset(
            data_list_path=self.hparams.test_list_path, eval_frames=self.hparams.eval_frames, num_eval=0)
        #! 为了测试同一张图片的可视化，暂时不shuffle
        loader = DataLoader(
            test_dataset, num_workers=self.hparams.num_workers, batch_size=1,shuffle=False)
        return loader
    
    def cosine_evaluate(self):
        eval_loader = self.test_dataloader(self.trials)
        index_mapping = {}
        eval_vectors = [[] for _ in range(len(eval_loader))]
        print("extract eval speaker embedding...")
        self.speaker_encoder.eval()
        with torch.no_grad():
            for idx, (data, label) in enumerate(tqdm(eval_loader)):
                data = data.permute(1, 0, 2).cuda()
                label = list(label)[0]
                index_mapping[label] = idx
                embedding = self.extract_speaker_embedding(data)
                embedding = torch.mean(embedding, axis=0)
                embedding = embedding.cpu().detach().numpy()
                eval_vectors[idx] = embedding
        eval_vectors = np.array(eval_vectors)
        print("scoring...")
        eer, th, mindcf_e, mindcf_h = cosine_score(self.trials, self.hparams.scores_path, index_mapping, eval_vectors)
        print("Cosine EER: {:.3f}%  minDCF(0.01): {:.5f}  minDCF(0.001): {:.5f}".format(eer*100, mindcf_e, mindcf_h))
        self.log('cosine_eer', eer*100)
        self.log('minDCF(0.01)', mindcf_e)
        self.log('minDCF(0.001)', mindcf_h)
        return eer, th, mindcf_e, mindcf_h

    def evaluate(self):
        dev_dataset = Dev_Dataset(data_list_path=self.hparams.dev_list_path, eval_frames=self.hparams.eval_frames, num_eval=0)
        dev_loader = DataLoader(dev_dataset, num_workers=self.hparams.num_workers, batch_size=1)

        # first we extract dev speaker embedding
        dev_vectors = []
        dev_labels = []
        print("extract dev speaker embedding...")
        self.speaker_encoder.eval()
        with torch.no_grad():
            for data, label in tqdm(dev_loader):
                length = len(data)
                embedding = self.extract_speaker_embedding(data.cuda())
                embedding = embedding.reshape(length, 1, -1)
                embedding = torch.mean(embedding, axis=1)
                embedding = embedding.cpu().detach().numpy()
                dev_vectors.append(embedding)
                label = label.cpu().detach().numpy()
                dev_labels.append(label)

        dev_vectors = np.vstack(dev_vectors).reshape(-1, self.hparams.embedding_dim)
        dev_labels = np.hstack(dev_labels)
        print("dev vectors shape:", dev_vectors.shape)
        print("dev labels shape:", dev_labels.shape)

        eval_loader = self.test_dataloader(self.trials)
        index_mapping = {}
        eval_vectors = [[] for _ in range(len(eval_loader))]
        print("extract eval speaker embedding...")
        self.speaker_encoder.eval()
        with torch.no_grad():
            for idx, (data, label) in enumerate(tqdm(eval_loader)):
                data = data.permute(1, 0, 2).cuda()
                label = list(label)[0]
                index_mapping[label] = idx
                embedding = self.extract_speaker_embedding(data)
                embedding = torch.mean(embedding, axis=0)
                embedding = embedding.cpu().detach().numpy()
                eval_vectors[idx] = embedding

        eval_vectors = np.array(eval_vectors)
        print("eval_vectors shape is: ", eval_vectors.shape)

        print("scoring...")
        eer, th, mindcf_e, mindcf_h = cosine_score(self.trials, index_mapping, eval_vectors)
        print("Cosine EER: {:.3f}%  minDCF(0.01): {:.5f}  minDCF(0.001): {:.5f}".format(eer*100, mindcf_e, mindcf_h))
        self.log('cosine eer', eer*100)
        self.log('cosine minDCF(0.01)', mindcf_e)
        self.log('cosine minDCF(0.001)', mindcf_h)

        # PLDA
        plda = PldaAnalyzer(n_components=self.hparams.plda_dim)
        plda.fit(dev_vectors, dev_labels, num_iter=10)
        eval_vectors_trans = plda.transform(eval_vectors)
        eer, th, mindcf_e, mindcf_h = PLDA_score(self.trials, index_mapping, eval_vectors_trans, plda)
        print("PLDA EER: {:.3f}%  minDCF(0.01): {:.5f}  minDCF(0.001): {:.5f}".format(eer*100, mindcf_e, mindcf_h))
        self.log('plda eer', eer*100)
        self.log('plda minDCF(0.01)', mindcf_e)
        self.log('plda minDCF(0.001)', mindcf_h)


    def evaluate_classfication(self):
        # 这部分的eval和确认就不一样了，把这个任务当成0-9十个数字识别的任务,最终输出的是acc以及某一句话的后验概率
        eval_loader = self.test_dataloader_for_classification()
        eval_posterior = []
        predict_classes = []
        wav_path = []
        right_num = 0
        print("echo eval posteriori probability...")
        self.speaker_encoder.eval()
        self.loss.eval()
        with torch.no_grad():
            # for idx, (data, label) in enumerate(tqdm(eval_loader)):
            for idx, (data, label, name) in enumerate(tqdm(eval_loader)):
                data = data.permute(1, 0, 2).cuda() # [1, 1, 81953] -> [1, 1, 81953] ref: evluate()
                label = label.cuda() # [1, 1, 81953] -> [1, 1, 81953] ref: evluate()

                #classification process
                logit = self.extract_speaker_embedding(data)
                logit = logit.unsqueeze(0)
                x = self.loss(logit)
                # x = self.softmax(logit) # logit.size() -> [1,400]
                x = x.cpu().numpy().tolist() # max(x[0]) is the max posterior
                label = label.cpu().numpy().tolist()[0]
                y_tilde = x[0].index(max(x[0]))
                # print("{},{}".format(y_tilde, label))
                if label == y_tilde :
                    right_num += 1
                    acc = (right_num / (idx+1)) * 100
                # Save the posterior corresponding to each wav into result.csv
                name = list(name)[0].split('/')[-1]
                wav_path.append(name)
                eval_posterior.append('{:.10f}'.format(x[0][label]))
                predict_classes.append(y_tilde)

        print("acc is {}%".format(acc))
        csv_dict = {"wav_path": wav_path,
                "eval_posterior": eval_posterior,
                "predict_classes": predict_classes,
                }
        df = pd.DataFrame(data=csv_dict)
        result_path  =  self.hparams.result_list_path
        try:
            df.to_csv(result_path)
            print(f'Saved data list file at {result_path}')
        except OSError as err:
            print(
                f'Ran in an error while saving {result_path}: {err}')

    def eval_forward(self,input,label):
        logit = self.speaker_encoder(input)
        logit = logit.unsqueeze(0)
        output = self.loss(logit,label)
        # output = self.softmax(output) # logit.size() -> [1,400]
        output = output.cpu().detach().numpy().tolist() # max(x[0]) is the max posterior
        label_int = label.cpu().numpy().tolist()[0]
        score = output[0][label]
        # print(score)
        # x[0].sort(reverse=True)
        # print(x[0])
        y_tilde = output[0].index(max(output[0]))
        # print(label_int, y_tilde)
        return score, label_int, y_tilde


    def evaluate_visualization(self,speaker_classification_model):
        eval_loader = self.test_dataloader_for_classification()
        self.speaker_encoder.eval()
        self.loss.eval()
        similarity_CAM2Grad_list,similarity_CAM2Gradpp_list,similarity_CAM2Score_list,similarity_CAM2Layer_list = [],[],[],[]
        idx_list,name_list,posterior_variance_list_00,posterior_variance_list_01,posterior_variance_list_02,posterior_variance_list_03,posterior_variance_list_04,posterior_input = [],[],[],[],[],[],[],[]
        # if self.hparams.target_layer == 'layer4':
        #     target_layers_name = ['layer4','layer4_SEBasicBlock2_conv1','layer4_SEBasicBlock2_bn1','layer4_SEBasicBlock2_conv2','layer4_SEBasicBlock2_bn2','layer4_SEBasicBlock2_relu','layer4_SEBasicBlock2_se'] # 设置一些layer_name,这些name的命名请看 def find_speaker_encoder_layer
        # elif self.hparams.target_layer == 'layer3':
        #     target_layers_name = ['layer3','layer3_SEBasicBlock5_conv1','layer3_SEBasicBlock5_bn1','layer3_SEBasicBlock5_conv2','layer3_SEBasicBlock5_bn2','layer3_SEBasicBlock5_relu','layer3_SEBasicBlock5_se'] # 设置一些layer_name,这些name的命名请看 def find_speaker_encoder_layer
        # elif self.hparams.target_layer == 'layer2':
        #     target_layers_name = ['layer2','layer2_SEBasicBlock3_conv1','layer2_SEBasicBlock3_bn1','layer2_SEBasicBlock3_conv2','layer2_SEBasicBlock3_bn2','layer2_SEBasicBlock3_relu','layer2_SEBasicBlock3_se'] # 设置一些layer_name,这些name的命名请看 def find_speaker_encoder_layer
        # target_layers_name = ['layer1','layer2','layer3','layer4']
        target_layers_name = ['layer4']
        for idx, (data, label, name) in enumerate(tqdm(eval_loader)):
            data = data.permute(1, 0, 2).cuda()
            label = label.cuda()
            # todo data preprocess
            x = data.reshape(-1, data.size()[-1])
            x = self.mel_trans(x) + 1e-6
            x = x.log()
            #todo test and draw mel-spectrum for input
            input_mel_spectrogram = self.instancenorm(x)
            input_target_class_posterior, label_int, y_tilde = self.eval_forward(input_mel_spectrogram,label)
            print("target_class_posterior:{},label:{},predict:{}".format(input_target_class_posterior, label_int, y_tilde))
            plt.figure()
            plt.imshow(x[0,:,:].cpu().numpy())
            plt.title("{}_input;\nlabel:{},predict:{},score:{}".format(idx,label_int,y_tilde,input_target_class_posterior))
            plt.savefig('{}/{}_input_mel_spectrogram.png'.format(self.hparams.cam_result,idx), bbox_inches='tight')
            #todo 计算两个cam的相关性
            name_list.append(name)
            idx_list.append(idx)
            similarity_CAM2Grad,similarity_CAM2Gradpp,similarity_CAM2Score,similarity_CAM2Layer = self.compute_cam_method_similarity(label,input_mel_spectrogram,speaker_classification_model)
            similarity_CAM2Grad_list.append(similarity_CAM2Grad)
            similarity_CAM2Gradpp_list.append(similarity_CAM2Gradpp)
            similarity_CAM2Score_list.append(similarity_CAM2Score)
            similarity_CAM2Layer_list.append(similarity_CAM2Layer)

            for i in range(len(target_layers_name)):
              target_layer = find_speaker_encoder_layer(speaker_classification_model,target_layers_name[i])
              if self.hparams.visualization_type == 'CAM':
                wrapped_model = CAM(model=speaker_classification_model,loss=self.loss, target_layer=target_layer)
                # cam, _ = wrapped_model(input_mel_spectrogram)
                cam, _ = wrapped_model(input_mel_spectrogram,idx=label)
              elif self.hparams.visualization_type == 'GradCAM':
                wrapped_model = GradCAM(model=speaker_classification_model,loss=self.loss, target_layer=target_layer)
                # cam, _ = wrapped_model(input_mel_spectrogram)
                cam, _ = wrapped_model(input_mel_spectrogram,idx=label)
              elif self.hparams.visualization_type == 'GradCAMpp':
                wrapped_model = GradCAMpp(model=speaker_classification_model,loss=self.loss, target_layer=target_layer)
                # cam, _ = wrapped_model(input_mel_spectrogram)
                cam, _ = wrapped_model(input_mel_spectrogram,idx=label)
              elif self.hparams.visualization_type == 'ScoreCAM':
                wrapped_model = ScoreCAM(model=speaker_classification_model,loss=self.loss, target_layer=target_layer)
                # cam, _ = wrapped_model(input_mel_spectrogram)
                cam, _ = wrapped_model(input_mel_spectrogram,idx=label)
              else:
                break
              #todo 验证class activation map靠不靠谱
              target_class_posterior, label_int, y_tilde = self.eval_forward((input_mel_spectrogram * cam[0,:,:,:]),label)
              print("target_class_posterior:{},label:{},predict:{}".format(target_class_posterior, label_int, y_tilde))
              #todo 根据class activation map 画热力图
              heat_map("{}_{};\nlabel:{},predict:{},score:{}".format(self.hparams.visualization_type, target_layers_name[i],label_int,y_tilde,target_class_posterior), \
                input_mel_spectrogram.cpu(), cam.type(torch.FloatTensor).cpu(), \
                save_path='{}/{}_heat_map_{}_{}.png'.format(self.hparams.cam_result,idx,self.hparams.visualization_type,target_layers_name[i]))
              print("The importance map of {} is saved as {}/{}_heat_map_{}_{}.png".format(self.hparams.visualization_type,self.hparams.cam_result,idx,self.hparams.visualization_type,target_layers_name[i]))
              
            if self.hparams.visualization_type == 'LayerCAM':
                gama = 2
                target_layers_name = ['layer1','layer2','layer3','layer4']
                cam_merge = 0
                for i in range(len(target_layers_name)):
                    target_layer = find_speaker_encoder_layer(speaker_classification_model,target_layers_name[i])
                    wrapped_model = LayerCAM(model=speaker_classification_model, loss=self.loss, target_layer=target_layer)
                    # cam, _ = wrapped_model(input_mel_spectrogram)
                    cam, _ = wrapped_model(input_mel_spectrogram,idx=label)
                    cam_merge = cam+cam_merge
                    # print(cam_merge)
                cam_merge = torch.tanh(gama * cam_merge / cam_merge.max())
                # todo 对这个 cam_merge做处理 设置一个阈值，低于阈值全变成0
                # cam_merge = torch.where(cam_merge > 0.3, 1, 0)
                
                #todo 验证class activation map靠不靠谱
                mergecam_target_class_posterior, label_int, y_tilde = self.eval_forward((input_mel_spectrogram * cam_merge[0,:,:,:]),label)
                print("target_class_posterior:{},label:{},predict:{}".format(mergecam_target_class_posterior, label_int, y_tilde))
                #todo 根据class activation map 画热力图
                heat_map("{}_merge_multi_layer threshold:0.3;\nlabel:{},predict:{},score:{}".format(self.hparams.visualization_type,label_int,y_tilde,mergecam_target_class_posterior), \
                  input_mel_spectrogram.cpu(), cam_merge.type(torch.FloatTensor).cpu(), \
                    save_path='{}/{}_heat_map_{}_{}.png'.format(self.hparams.cam_result,idx,self.hparams.visualization_type,target_layers_name[i]))
                print("The importance map of {} is saved as {}/{}_heat_map_{}_{}.png".format(self.hparams.visualization_type,self.hparams.cam_result,idx,self.hparams.visualization_type,target_layers_name[i]))

        #         #todo 对这个 cam_merge做处理 设置一个阈值，低于阈值全变成0
        #         lamdas = np.linspace(0,0.4,5)
        #         name_list.append(name)
        #         idx_list.append(idx)
        #         for j in range(len(lamdas)):
        #           b=torch.zeros(cam_merge.shape).cuda()
        #           cam_merge_test = cam_merge.clone()
        #           cam_merge_ = torch.where(cam_merge_test > lamdas[j], cam_merge_test, b)
        #           #todo 验证class activation map靠不靠谱
        #           mergecam_target_class_posterior, label_int, y_tilde = self.eval_forward((input_mel_spectrogram * cam_merge_[0,:,:,:]),label)
        #           print("target_class_posterior:{},label:{},predict:{}".format(mergecam_target_class_posterior, label_int, y_tilde))
        #           #todo 观察不同的阈值，对于target_class_posterior 的变化趋势。不同例子可不可以有一个相同的趋势，如果有反常，那么在哪里？
        #           variance_posterior = mergecam_target_class_posterior - input_target_class_posterior
        #           if j == 0:
        #             posterior_variance_list_00.append(variance_posterior)
        #           if j == 1:
        #             posterior_variance_list_01.append(variance_posterior)
        #           if j == 2:
        #             posterior_variance_list_02.append(variance_posterior)
        #           if j == 3:
        #             posterior_variance_list_03.append(variance_posterior)
        #           if j == 4:
        #             posterior_variance_list_04.append(variance_posterior)

        # csv_dict = {"idx":idx_list,
        #         "name_list": name_list,
        #         "th=0": posterior_variance_list_00,
        #         "th=0.1": posterior_variance_list_01,
        #         "th=0.2": posterior_variance_list_02,
        #         "th=0.3": posterior_variance_list_03,
        #         "th=0.4": posterior_variance_list_04,
        #         }
        # df = pd.DataFrame(data=csv_dict)
        # result_path  =  self.hparams.result_list_path
        # try:
        #     df.to_csv(result_path)
        #     print(f'Saved data list file at {result_path}')
        # except OSError as err:
        #     print(
        #         f'Ran in an error while saving {result_path}: {err}')      
        csv_dict = {"idx":idx_list,
                "name_list": name_list,
                "similarity_CAM2Grad": similarity_CAM2Grad_list,
                "similarity_CAM2Gradpp": similarity_CAM2Gradpp_list,
                "similarity_CAM2Score": similarity_CAM2Score_list,
                "similarity_CAM2Layer": similarity_CAM2Layer_list,
                }
        df = pd.DataFrame(data=csv_dict)
        result_path  =  self.hparams.result_list_path
        try:
            df.to_csv(result_path)
            print(f'Saved data list file at {result_path}')
        except OSError as err:
            print(
                f'Ran in an error while saving {result_path}: {err}')  


    def explanation_black_box_By_RISE(self,speaker_classification_model):
        from .cam.rise import RISE

        eval_loader = self.test_dataloader_for_classification()
        self.speaker_encoder.eval()
        self.loss.eval()

        for idx, (data, label, name) in enumerate(tqdm(eval_loader)):
            data = data.permute(1, 0, 2).cuda()
            label = label.cuda()
            # todo data preprocess
            x = data.reshape(-1, data.size()[-1])
            x = self.mel_trans(x) + 1e-6
            x = x.log()
            #todo test and draw mel-spectrum for input
            input_mel_spectrogram = self.instancenorm(x)
            plt.figure()
            plt.imshow(x[0,:,:].cpu().numpy())
            # plt.title("{}_input;\nlabel:{},predict:{},score:{}".format(idx,label_int,y_tilde,input_target_class_posterior))
            plt.savefig('input_mel_spectrogram.png')
            input_size = input_mel_spectrogram.squeeze(0).shape
            gpu_batch = 150 # use GUP for parallel compute input multiple with many masks.
            workers = 8   # jobs number
            with torch.no_grad():       # black_box do not need gradient and internal information       
                explainer = RISE(self.speaker_encoder, self.loss, input_size, gpu_batch) #! 这部分 要修改一下 RISE类 ，因为model传进去还需要根据说话人识别系统进行修改才能保证正确的前向过程。
                
                #todo Generate masks for RISE or use the saved ones.
                maskspath = 'masks_for_example_1.npy'
                generate_new = True 
                if generate_new or not os.path.isfile(maskspath):
                    explainer.generate_masks(N=6000, s=8, p1=0.1, savepath=maskspath)
                else:
                    explainer.load_masks(maskspath)
                    print('Masks are loaded.')
                
                saliency = explainer(input_mel_spectrogram,label)
                #todo 对saliency map做处理 直接乘是不可以的
                input_target_class_posterior, label_int, y_tilde = self.eval_forward(input_mel_spectrogram,label)
                print("target_class_posterior:{},label:{},predict:{}".format(input_target_class_posterior, label_int, y_tilde))
                input_target_class_posterior, label_int, y_tilde = self.eval_forward(input_mel_spectrogram*saliency[0],label)
                print("target_class_posterior:{},label:{},predict:{}".format(input_target_class_posterior, label_int, y_tilde))
                saliency =  saliency.cpu().numpy()
                plt.figure()
                plt.imshow(saliency[0], cmap='jet', alpha=0.5)
                plt.colorbar(fraction=0.046, pad=0.04)
                plt.savefig('test_RISE.png')

                
    def evaluation_for_RISE(self,speaker_classification_model):
        from .cam.rise import RISE
        from .cam.evaluation_RISE import CausalMetric, auc, gkern
        from torch.nn.functional import conv2d

        eval_loader = self.test_dataloader_for_classification()
        self.speaker_encoder.eval()
        self.loss.eval()

        for idx, (data, label, name) in enumerate(tqdm(eval_loader)):
            data = data.permute(1, 0, 2).cuda()
            label = label.cuda()
            # todo data preprocess
            x = data.reshape(-1, data.size()[-1])
            x = self.mel_trans(x) + 1e-6
            x = x.log()
            #todo test and draw mel-spectrum for input
            input_mel_spectrogram = self.instancenorm(x)
            input_size = input_mel_spectrogram.squeeze(0).shape
            plt.figure()
            plt.imshow(x[0,:,:].cpu().numpy())
            # plt.colorbar(fraction=0.046, pad=0.04)
            # plt.title("{}_input;\nlabel:{},predict:{},score:{}".format(idx,label_int,y_tilde,input_target_class_posterior))
            plt.savefig('input_mel_spectrogram.png')
            gpu_batch = 150 # use GUP for parallel compute input multiple with many masks.
            workers = 8   # jobs number
            with torch.no_grad():       # black_box do not need gradient and internal information       
                explainer = RISE(self.speaker_encoder, self.loss, input_size, gpu_batch) #! 这部分 要修改一下 RISE类 ，因为model传进去还需要根据说话人识别系统进行修改才能保证正确的前向过程。
                
                #todo Generate masks for RISE or use the saved ones.
                maskspath = 'masks_for_example_1.npy'
                generate_new = True 
                if generate_new or not os.path.isfile(maskspath):
                    explainer.generate_masks(N=8000, s=8, p1=0.1, savepath=maskspath)
                else:
                    explainer.load_masks(maskspath)
                    print('Masks are loaded.')
                
                saliency = explainer(input_mel_spectrogram,label)[[0]].cpu().numpy()
                # import ipdb ; ipdb.set_trace()
                # saliency =  saliency.cpu().numpy()
                plt.figure()
                plt.imshow(saliency[0], cmap='jet', alpha=0.5)
                plt.colorbar(fraction=0.046, pad=0.04)
                plt.savefig('test_RISE.png')
                
                klen = 11
                ksig = 5
                kern = gkern(klen, ksig)

                # Function that blurs input image
                # lambda 冒号之前是函数的输入参数, 冒号之后是函数体
                blur = lambda x: nn.functional.conv2d(x, kern, padding=klen//2)
                insertion = CausalMetric(self.speaker_encoder, self.loss, input_size, 'ins', 80, substrate_fn=blur)
                deletion = CausalMetric(self.speaker_encoder, self.loss, input_size, 'del', 80, substrate_fn=torch.zeros_like)
                h = insertion.single_run(input_mel_spectrogram, label, saliency, verbose=2,save_to='insertion')
                h = deletion.single_run(input_mel_spectrogram, label, saliency, verbose=2,save_to='deletion')
    
    def evaluation_for_Layer_CAM(self,speaker_classification_model):
        from .cam.evaluation_RISE import CausalMetric, auc, gkern
        from torch.nn.functional import conv2d
        eval_loader = self.test_dataloader_for_classification()
        self.speaker_encoder.eval()
        self.loss.eval()

        for idx, (data, label, name) in enumerate(tqdm(eval_loader)):
            data = data.permute(1, 0, 2).cuda()
            label = label.cuda()
            # todo data preprocess
            x = data.reshape(-1, data.size()[-1])
            x = self.mel_trans(x) + 1e-6
            x = x.log()
            #todo test and draw mel-spectrum for input
            input_mel_spectrogram = self.instancenorm(x)
            input_size = input_mel_spectrogram.squeeze(0).shape
            input_target_class_posterior, label_int, y_tilde = self.eval_forward(input_mel_spectrogram,label)
            plt.figure()
            plt.imshow(x[0,:,:].cpu().numpy())
            plt.title("{}_input;\nlabel:{},predict:{},score:{}".format(idx,label_int,y_tilde,input_target_class_posterior))
            plt.savefig('input_mel_spectrogram.png')
            
            if self.hparams.visualization_type == 'LayerCAM':
                gama = 2
                target_layers_name = ['layer1','layer2','layer3','layer4']
                cam_merge = 0
                for i in range(len(target_layers_name)):
                    target_layer = find_speaker_encoder_layer(speaker_classification_model,target_layers_name[i])
                    wrapped_model = LayerCAM(model=speaker_classification_model, loss=self.loss, target_layer=target_layer)
                    # cam, _ = wrapped_model(input_mel_spectrogram)
                    cam, _ = wrapped_model(input_mel_spectrogram,idx=label)
                    cam_merge = cam+cam_merge
                    # print(cam_merge)
                cam_merge = torch.tanh(gama * cam_merge / cam_merge.max())
                
                #todo 验证class activation map靠不靠谱
                mergecam_target_class_posterior, label_int, y_tilde = self.eval_forward((input_mel_spectrogram * cam_merge[0,:,:,:]),label)
                print("target_class_posterior:{},label:{},predict:{}".format(mergecam_target_class_posterior, label_int, y_tilde))
                #todo 根据class activation map 画热力图
                heat_map("{}_merge_multi_layer ;\nlabel:{},predict:{},score:{}".format(self.hparams.visualization_type,label_int,y_tilde,mergecam_target_class_posterior), \
                  input_mel_spectrogram.cpu(), cam_merge.type(torch.FloatTensor).cpu(), \
                    save_path='Demo/{}_heat_map_{}_{}.png'.format(idx,self.hparams.visualization_type,target_layers_name[i]))
                print("The importance map of {} is saved as {}/{}_heat_map_{}_{}.png".format(self.hparams.visualization_type,self.hparams.cam_result,idx,self.hparams.visualization_type,target_layers_name[i]))

                klen = 11
                ksig = 5
                kern = gkern(klen, ksig)
                # Function that blurs input image
                # lambda 冒号之前是函数的输入参数, 冒号之后是函数体
                blur = lambda x: nn.functional.conv2d(x, kern, padding=klen//2)
                insertion = CausalMetric(self.speaker_encoder, self.loss, input_size, 'ins', 80, substrate_fn=blur)
                deletion = CausalMetric(self.speaker_encoder, self.loss, input_size, 'del', 80, substrate_fn=torch.zeros_like)
                h = insertion.single_run(input_mel_spectrogram, label, cam_merge[0].cpu().numpy(), verbose=2,save_to='Demo/Layer_insertion')
                h = deletion.single_run(input_mel_spectrogram, label, cam_merge[0].cpu().numpy(), verbose=2,save_to='Demo/Layer_deletion')


    def compute_cam_method_similarity(self,label,input_mel_spectrogram,speaker_classification_model):
        from torch import cosine_similarity
        """计算方法:计算每一行的的cosine相似度得出(1x80)的相似度矩阵，对其求平均。得到一个相似度的数"""
        # target_layers_name = ['layer1','layer2','layer3','layer4']
        target_layers_name = ['layer4']
        for i in range(len(target_layers_name)):
            target_layer = find_speaker_encoder_layer(speaker_classification_model,target_layers_name[i])
            wrapped_model = CAM(model=speaker_classification_model,loss=self.loss, target_layer=target_layer)
            # CAM_cam, _ = wrapped_model(input_mel_spectrogram)
            CAM_cam, _ = wrapped_model(input_mel_spectrogram,idx=label)

            wrapped_model = GradCAM(model=speaker_classification_model,loss=self.loss, target_layer=target_layer)
            # Grad_CAM_cam, _ = wrapped_model(input_mel_spectrogram)
            Grad_CAM_cam, _ = wrapped_model(input_mel_spectrogram,idx=label)

            wrapped_model = GradCAMpp(model=speaker_classification_model,loss=self.loss, target_layer=target_layer)
            # Grad_CAMpp_cam, _ = wrapped_model(input_mel_spectrogram)
            Grad_CAMpp_cam, _ = wrapped_model(input_mel_spectrogram,idx=label)

            wrapped_model = ScoreCAM(model=speaker_classification_model,loss=self.loss, target_layer=target_layer)
            # Score_CAM_cam, _ = wrapped_model(input_mel_spectrogram)
            Score_CAM_cam, _ = wrapped_model(input_mel_spectrogram,idx=label)
            gama = 2
            target_layers_name = ['layer1','layer2','layer3','layer4']
            cam_merge = 0
            for i in range(len(target_layers_name)):
                target_layer = find_speaker_encoder_layer(speaker_classification_model,target_layers_name[i])
                wrapped_model = LayerCAM(model=speaker_classification_model, loss=self.loss, target_layer=target_layer)
                # cam, _ = wrapped_model(input_mel_spectrogram)
                cam, _ = wrapped_model(input_mel_spectrogram,idx=label)
                cam_merge = cam+cam_merge
                # print(cam_merge)
            Layer_cam_merge = torch.tanh(gama * cam_merge / cam_merge.max())
            similarity_CAM2Grad = cosine_similarity(CAM_cam[0,0,:,:], Grad_CAM_cam[0,0,:,:]).mean().cpu().tolist()
            similarity_CAM2Gradpp = cosine_similarity(CAM_cam[0,0,:,:], Grad_CAMpp_cam[0,0,:,:]).mean().cpu().tolist()
            similarity_CAM2Score = cosine_similarity(CAM_cam[0,0,:,:], Score_CAM_cam[0,0,:,:]).mean().cpu().tolist()
            similarity_CAM2Layer = cosine_similarity(CAM_cam[0,0,:,:], Layer_cam_merge[0,0,:,:]).mean().cpu().tolist()

            # similarity = CAM_cam[0,0,:,:].mm(Score_CAM_cam[0,0,:,:].t())
            print(similarity_CAM2Grad,similarity_CAM2Gradpp,similarity_CAM2Score,similarity_CAM2Layer)
            return similarity_CAM2Grad,similarity_CAM2Gradpp,similarity_CAM2Score,similarity_CAM2Layer

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.hparams.learning_rate)
        lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=self.hparams.lr_step_size, gamma=self.hparams.lr_gamma)
        print("init {} optimizer with learning rate {}".format("Adam", self.hparams.learning_rate))
        print("init Step lr_scheduler with step size {} and gamma {}".format(self.hparams.lr_step_size, self.hparams.lr_gamma))
        return [optimizer], [lr_scheduler]

    def optimizer_step(self, epoch, batch_idx, optimizer, optimizer_idx,
                   optimizer_closure, on_tpu, using_native_amp, using_lbfgs):
        # warm up learning_rate
        if self.trainer.global_step < 500:
            lr_scale = min(1., float(self.trainer.global_step + 1) / 500.)
            for idx, pg in enumerate(optimizer.param_groups):
                pg['lr'] = lr_scale * self.hparams.learning_rate
        # update params
        optimizer.step(closure=optimizer_closure)
        optimizer.zero_grad()

    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = ArgumentParser(parents=[parent_parser], add_help=False)

        # Data Loader       
        parser.add_argument('--max_frames', type=int, default=201)
        parser.add_argument('--min_frames', type=int, default=200)
        parser.add_argument('--eval_frames', type=int, default=0)
        parser.add_argument('--batch_size', type=int, default=32)
        parser.add_argument('--max_seg_per_spk', type=int, default=500, help='Maximum number of utterances per speaker per epoch')
        parser.add_argument('--nPerSpeaker', type=int, default=1, help='Number of utterances per speaker per batch, only for metric learning based losses');
        parser.add_argument('--num_workers', type=int, default=16)
        parser.add_argument('--augment', action='store_true', default=False)

        # Training details
        parser.add_argument('--max_epoch', type=int, default=500, help='Maximum number of epochs')
        parser.add_argument('--loss_type', type=str, default="softmax")
        parser.add_argument('--nnet_type', type=str, default="ResNetSE34L")
        parser.add_argument('--pooling_type', type=str, default="SAP")
        parser.add_argument('--eval_interval', type=int, default=1)
        parser.add_argument('--keep_loss_weight', action='store_true', default=False)

        # Optimizer
        parser.add_argument('--learning_rate', type=float, default=0.001)
        parser.add_argument('--lr_step_size', type=int, default=10)
        parser.add_argument('--lr_gamma', type=float, default=0.95)

        # Loss functions
        parser.add_argument('--margin', type=float, default=0.2)
        parser.add_argument('--scale', type=float, default=30)

        # Training and test data
        parser.add_argument('--train_list_path', type=str, default='')
        parser.add_argument('--dev_list_path', type=str, default='')
        parser.add_argument('--test_list_path', type=str, default='')
        parser.add_argument('--trials_path', type=str, default='trials.lst')
        parser.add_argument('--scores_path', type=str, default='scores.lst')
        parser.add_argument('--musan_list_path', type=str, default='')
        parser.add_argument('--rirs_list_path', type=str, default='')
        parser.add_argument('--result_list_path', type=str, default='')

        # Load and save
        parser.add_argument('--checkpoint_path', type=str, default=None)
        parser.add_argument('--save_top_k', type=int, default=15)
        parser.add_argument('--suffix', type=str, default='')

        # Model definition
        parser.add_argument('--n_mels', type=int, default=80)
        parser.add_argument('--embedding_dim', type=int, default=256)
        parser.add_argument('--plda_dim', type=int, default=256)

        # Test mode
        parser.add_argument('--evaluate', action='store_true', default=False)
        parser.add_argument('--classification', action='store_true', default=False)
        parser.add_argument('--verification', action='store_true', default=False)

        #visualization
        parser.add_argument('--visualization', action='store_true', default=False)
        parser.add_argument('--visualization_type', type=str, default='CAM')
        parser.add_argument('--target_layer', type=str, default='layer4')
        parser.add_argument('--cam_result', type=str, default='Layer_cam_result_mask')
        
        return parser

