import numpy as np
import torch
import torch.nn as nn
from skimage.transform import resize
from tqdm import tqdm
import sys  
import matplotlib.pyplot as plt

class RISE(nn.Module):
    def __init__(self, model,loss, input_size, gpu_batch=100):
        super(RISE, self).__init__()
        self.model = model
        self.loss = loss
        self.input_size = input_size
        self.gpu_batch = gpu_batch

    def generate_masks(self, N, s, p1, savepath='masks.npy'):
        cell_size = np.ceil(np.array(self.input_size) / s)
        up_size = (s + 1) * cell_size

        grid = np.random.rand(N, s, s) < p1
        grid = grid.astype('float32')

        self.masks = np.empty((N, *self.input_size))

        for i in tqdm(range(N), desc='Generating filters'):
            # Random shifts
            x = np.random.randint(0, cell_size[0])
            y = np.random.randint(0, cell_size[1])
            # Linear upsampling and cropping
            self.masks[i, :, :] = resize(grid[i], up_size, order=1, mode='reflect',
                                         anti_aliasing=False)[x:x + self.input_size[0], y:y + self.input_size[1]]
        self.masks = self.masks.reshape(-1, *self.input_size)
        np.save(savepath, self.masks)
        self.masks = torch.from_numpy(self.masks).float()
        self.masks = self.masks.cuda()
        self.N = N
        self.p1 = p1

    def load_masks(self, filepath):
        self.masks = np.load(filepath)
        self.masks = torch.from_numpy(self.masks).float().cuda()
        self.N = self.masks.shape[0]

    def forward(self, x, label):
        N = self.N
        _, H, W = x.size()
        # Apply array of filters to the image
        stack = torch.mul(self.masks, x.data)  # stack torch.Size([6000,80,641])
        #todo 验证一下
        for i in range(10):
            plt.figure()
            plt.imshow(stack[i,:,:].cpu().numpy())
            plt.title("inpute_by_{}th_mask".format(i))
            plt.savefig("inpute_by_{}th_mask".format(i))
 
        # p = nn.Softmax(dim=1)(model(stack)) processed in batches
        logits = []
        for i in range(0, N, self.gpu_batch):
            logits.append(self.model(stack[i:min(i + self.gpu_batch, N)]))
        #[40,150,256]    len(logits):40(number of stack : all_mask/gpu_batch) len(logits[0]):150(number of embeddings with a stack)    len(logits[0][0]):256(embedding) 
        
        logits = torch.cat(logits)
        print(label.shape)
        # label.expand(10)
        label = label.repeat(N,1)

        #[6000,256]    len(logits):6000(number of all_mask)  len(logits[0]):256(the dimension of embedding) 
        logits = logits.unsqueeze(0)
        p = self.loss(logits,label)
        
        # Number of classes
        CL = p.size(1)
        # print(p.data.shape) # p.data.shape = torch.Size([6000, 5994]) 代表6000个mask，每个mask对应的都会产生5994个类别的概率
        # print(p.data.transpose(0,1).shape) # p.data.transpose(0,1).shape = torch.Size([5994, 6000]) 代表每一个类别，6000个mask的概率
        # todo 下面这一行， 每一类的6000个mask 与在这一类中的概率相乘，然后相加。得到这一类的saliency_map。总共5994个类，每一个saliency_map的大侠H*W
        sal = torch.matmul(p.data.transpose(0, 1), self.masks.view(N, H * W))
        # print(sal.shape) # sal.shape = torch.Size([5994,H*W]) 
        sal = sal.view((CL, H, W))
        sal = sal / N / self.p1
        return sal
    
    
class RISEBatch(RISE):
    def forward(self, x):
        # Apply array of filters to the image
        N = self.N
        B, C, H, W = x.size()
        stack = torch.mul(self.masks.view(N, 1, H, W), x.data.view(B * C, H, W))
        stack = stack.view(B * N, C, H, W)
        stack = stack

        #p = nn.Softmax(dim=1)(model(stack)) in batches
        p = []
        for i in range(0, N*B, self.gpu_batch):
            p.append(self.model(stack[i:min(i + self.gpu_batch, N*B)]))
        p = torch.cat(p)
        CL = p.size(1)
        p = p.view(N, B, CL)
        sal = torch.matmul(p.permute(1, 2, 0), self.masks.view(N, H * W))
        sal = sal.view(B, CL, H, W)
        return sal

# To process in batches
# def explain_all_batch(data_loader, explainer):
#     n_batch = len(data_loader)
#     b_size = data_loader.batch_size
#     total = n_batch * b_size
#     # Get all predicted labels first
#     target = np.empty(total, 'int64')
#     for i, (imgs, _) in enumerate(tqdm(data_loader, total=n_batch, desc='Predicting labels')):
#         p, c = torch.max(nn.Softmax(1)(explainer.model(imgs.cuda())), dim=1)
#         target[i * b_size:(i + 1) * b_size] = c
#     image_size = imgs.shape[-2:]
#
#     # Get saliency maps for all images in val loader
#     explanations = np.empty((total, *image_size))
#     for i, (imgs, _) in enumerate(tqdm(data_loader, total=n_batch, desc='Explaining images')):
#         saliency_maps = explainer(imgs.cuda())
#         explanations[i * b_size:(i + 1) * b_size] = saliency_maps[
#             range(b_size), target[i * b_size:(i + 1) * b_size]].data.cpu().numpy()
#     return explanations
